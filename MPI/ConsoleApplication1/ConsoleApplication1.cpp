﻿#include <iostream>
#include <iomanip>
#include <mpi.h>
#include <stdio.h>
#include "windows.h" 
#include <time.h>
#include "functions.h"


#define FIRST_THREAD 0
int FACTOR = 0;


using namespace std;


void main()
{
    MPI_Init(NULL, NULL);
    MPI_Status status;
    int n_cors; MPI_Comm_size(MPI_COMM_WORLD, &n_cors);
    int n_process; MPI_Comm_rank(MPI_COMM_WORLD, &n_process);
    int n;
    float* vector;

    // Send matrix size
    if (n_process == FIRST_THREAD) {
        srand((unsigned)time(NULL));
        cout << "Input matrix size: ";
        cin >> n;
        vector = InitVector(n * n);
        WriteMatrix(vector, n);
        MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(vector, n * n, MPI_FLOAT, FIRST_THREAD, MPI_COMM_WORLD);
    }
    else {
        MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
        vector = new float[n * n];
        MPI_Bcast(vector, n * n, MPI_FLOAT, FIRST_THREAD, MPI_COMM_WORLD);
    }
    // Составляем цепь процессов для обработки
    // Например 4 ядра всего, размер матрицы 5x5, процессы вычисления (1, 2, 3, 1, 2)
    int* execute_chain = new int[n];
    for (int i = 0, num_proc = 1; i < n; i++, num_proc++) {
        if (!(num_proc % n_cors))
            num_proc = 1;
        execute_chain[i] = num_proc % n_cors;
    }
    // convert vector to matrix in all processes
    float** matrix = ConvertVectorToMatrix(vector, n);
    /////////////////////////////////////
    // GetDeterminantParallelMode part
    /////////////////////////////////////
    float determinant = 0;
    bool find_determinant_flag = false;
    // Simple variants
    if (n < 1) {
        cout << "Wrong matrix size!!!" << endl;
        MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    }
    else if (n == 1) {
        determinant = matrix[0][0];
        find_determinant_flag = true;
    }
    else if (n == 2) {
        determinant = matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
        find_determinant_flag = true;
    }
    // Hard variants
    if (!find_determinant_flag)
        if (n_process == FIRST_THREAD) {
            clock_t tStart = clock();
            for (int i = 0; i < n; i++) {
                float value;
                MPI_Recv(&value, 1, MPI_FLOAT, execute_chain[i], execute_chain[i] + n_cors * FACTOR, MPI_COMM_WORLD, &status);
                determinant += value;
                if (execute_chain[i] == (n_cors - 1))
                    FACTOR += 1;
            }
            cout << "Determinant = " << determinant << endl;
            cout << "Time: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << " SECONDS" << endl;
        }
        else {
            int degree = 1;
            for (int i = 0; i < n; i++) {
                if (n_process == execute_chain[i]) {
                    int m = n - 1;
                    float** minor = GetMatrixMinor(matrix, n, m, 0, i);
                    float value = degree * matrix[0][i] * GetDeterminant(minor, m);
                    MPI_Send(&value, 1, MPI_FLOAT, 0, execute_chain[i] + n_cors * FACTOR, MPI_COMM_WORLD);
                    FACTOR += 1;
                }
                degree = -degree;
            }
        }
    MPI_Bcast(&determinant, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
    FACTOR = 0;
    /////////////////////////////////////
    // Inverse matrix execution part
    /////////////////////////////////////
    float** inverse_matrix;
    if (determinant) {
        if (n_process == FIRST_THREAD) {
            clock_t tStart = clock();
            float** inverse_matrix = InitMatrix(n);
            for (int i = 0; i < n; i++) {
                float* vector_values = new float[n];
                MPI_Recv(vector_values, n, MPI_FLOAT, execute_chain[i], execute_chain[i] + n_cors * FACTOR, MPI_COMM_WORLD, &status);
                inverse_matrix[i] = vector_values;
                if (execute_chain[i] == (n_cors - 1))
                    FACTOR += 1;
            }
            WriteMatrix(inverse_matrix, n, n);
            float** transponate_matrix = TransponateMatrix(inverse_matrix, n);
            WriteMatrix(transponate_matrix, n, n);
            cout << "Time: " << (double)(clock() - tStart) / CLOCKS_PER_SEC << " SECONDS" << endl;
        }
        else {
            int m = n - 1;
            for (int i = 0; i < n; i++) {
                if (n_process == execute_chain[i]) {
                    float* vector_values = new float[n];
                    for (int j = 0; j < n; j++) {
                        float** minor = GetMatrixMinor(matrix, n, m, i, j);
                        vector_values[j] = pow(-1.0, i + j + 2) * GetDeterminant(minor, m) / determinant;
                    }
                    MPI_Send(vector_values, n, MPI_FLOAT, 0, execute_chain[i] + n_cors * FACTOR, MPI_COMM_WORLD);
                    FACTOR += 1;
                }
            }
        }
    }
    else {
        if (n_process == FIRST_THREAD) {
            cout << "Because matrix determinant = 0,\nthen the matrix is degenerate and has no inverse!!!" << endl;
            MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
        }
    }
    MPI_Finalize();
 }