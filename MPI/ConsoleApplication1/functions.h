﻿using namespace std;


float** TransponateMatrix(float** matrix, int n);
float** GetMatrixMinor(float** matrix, int n, int m, int row_num, int col_num);
float GetDeterminant(float** matrix, int n);
float* InitVector(int size);
float** ConvertVectorToMatrix(float* vector, int size);
float** InitMatrix(int size);
void WriteMatrix(float** matrix, int m, int n);
void WriteMatrix(float* vector, int size);


//Функция транспонирования матрицы
float** TransponateMatrix(float** matrix, int n)
{
    float ** transponate_matrix = InitMatrix(n);
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            transponate_matrix[j][i] = matrix[i][j];
    return transponate_matrix;
}


// получение минора
float** GetMatrixMinor(float** matrix, int n, int m, int row_num, int col_num)
{
    float** minor = InitMatrix(m); // заполняемое значение неважно, потом поправить
    int k_i = 0;
    for (int i = 0; i < n; i++)
        if (i != row_num) {
            int k_j = 0;
            for (int j = 0; j < n; j++)
                if (j != col_num) {
                    minor[k_i][k_j] = matrix[i][j];
                    k_j++;
                }
            k_i++;
        }
    return minor;
}


float GetDeterminant(float** matrix, int n)
{
    if (n < 1) {
        cout << "Wrong matrix size!!!" << endl;
        return 0;
    }
    else if (n == 1)
        return matrix[0][0];
    else if (n == 2)
        return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
    else {
        float determinant = 0;
        int degree = 1;

        for (int i = 0; i < n; i++) {
            int m = n - 1;
            float** minor = GetMatrixMinor(matrix, n, m, 0, i);
            determinant += degree * matrix[0][i] * GetDeterminant(minor, m);
            degree = -degree;
        }
        return determinant;
    }
}


float* InitVector(int size)
{
    float* vector = new float[size];
    for (int i = 0; i < size; i++)
        vector[i] = rand() % 9 + 1;
    return vector;
}


float** ConvertVectorToMatrix(float* vector, int size)
{
    float** matrix = new float* [size];
    for (int i = 0; i < size; i++) {
        matrix[i] = new float[size];
        for (int j = 0; j < size; j++)
            matrix[i][j] = vector[j + i * size];
    }
    return matrix;
}


float** InitMatrix(int size) 
{
    float** matrix = new float* [size];
    for (int i = 0; i < size; i++) {
        matrix[i] = new float[size];
        for (int j = 0; j < size; j++) {
            matrix[i][j] = rand() % 9 + 1;
        }
    }
    return matrix;
}


void WriteMatrix(float** matrix, int m, int n)
{
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++)
            cout << matrix[i][j] << "\t";
        cout << '\n';
    }
    cout << '\n';
}


void WriteMatrix(float* vector, int size)
{
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++)
            cout << vector[i * size + j] << " ";
        cout << '\n';
    }
    cout << '\n';
}