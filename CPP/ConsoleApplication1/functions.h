﻿#include <vector>

using Matrix = vector<vector<double>>;

template <typename MatrixT> Matrix TransponateMatrix(MatrixT matrix, int n);

void InitMatrix(Matrix& matrix, size_t size, bool random_flag);
Matrix GetMatrixMinor(Matrix matrix, int n, int m, int indRow, int indCol);
double GetDeterminant(Matrix matrix, int n);


ostream& operator<<(ostream& os, const Matrix& matrix)
{
    os << fixed << setprecision(6);
    for (const auto& row : matrix)
    {
        for (const auto& item : row)
            os << setw(9) << item << ' ';
        os << '\n';
    }
    os << endl;

    return os;
};


void InitMatrix(Matrix& matrix, size_t size, bool random_flag=true)
{
    double init_value = 0;
    if (random_flag)
        srand((unsigned)time(NULL));

    matrix.reserve(size);
    for (size_t i = 0; i < size; ++i)
    {
        vector<double> row;
        double element = 0.;

        row.reserve(size);
        for (size_t j = 0; j < size; ++j)
        {
            if (random_flag)
                row.push_back(rand() % 9 + 1);
            else
                row.push_back(init_value);
        }
        matrix.push_back(move(row));
    }
}
//Функция транспонирования матрицы
template <typename MatrixT> Matrix TransponateMatrix(MatrixT matrix, int n)
{
    MatrixT transponate_matrix;
    InitMatrix(transponate_matrix, n, false);
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            transponate_matrix[j][i] = matrix[i][j];
    return transponate_matrix;
}
// получение минора
Matrix GetMatrixMinor(Matrix matrix, int n, int m, int row_num, int col_num)
{
    Matrix minor;
    InitMatrix(minor, m);

    int k_i = 0;
    for (int i = 0; i < n; i++) {
        if (i != row_num) {
            int k_j = 0;
            for (int j = 0; j < n; j++) {
                if (j != col_num) {
                    minor[k_i][k_j] = matrix[i][j];
                    k_j++;
                }
            }
            k_i++;
        }
    }
    return minor;
}
//функция вычисления определителя матрицы
double GetDeterminant(Matrix matrix, int n)
{
    if (n < 1) {
        cout << "Wrong matrix size!!!" << endl;
        return 0;
    }
    else if (n == 1)
        return matrix[0][0];
    else if (n == 2)
        return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
    else {
        double determinant = 0;
        int degree = 1;

        for (int i = 0; i < n; i++) {
            int m = n - 1;
            Matrix minor = GetMatrixMinor(matrix, n, m, 0, i);
            determinant += degree * matrix[0][i] * GetDeterminant(minor, m);
            degree = -degree;
        }
        return determinant;
    }
}