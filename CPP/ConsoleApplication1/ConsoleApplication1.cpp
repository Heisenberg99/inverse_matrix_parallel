﻿#include <iostream>
#include <future>
#include <vector>
#include <iomanip>

#include "windows.h" 
#include "profile.h"
#include "functions.h"

using Matrix = vector<vector<double>>;

using namespace std;


void UserMode();
void TestMode();
Matrix GetInverseMatrixParallelMode(Matrix matrix, int n, double determinant);
Matrix GetInverseMatrixSingleMode(Matrix matrix, int n, double determinant);
double GetDeterminantParallelMode(Matrix matrix, int n);
double GetDeterminantSingleMode(Matrix matrix, int n);

Matrix GetInverseMatrixParallelMode(Matrix matrix, int n, double determinant)
{
    LOG_DURATION("Func time: ");
    vector<future<double>> futures;
    futures.clear();

    Matrix inverse_matrix;
    InitMatrix(inverse_matrix, n, false);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            futures.push_back(async([&matrix, n, i, j, determinant]
                {
                    int m = n - 1;
                    Matrix minor = GetMatrixMinor(matrix, n, m, i, j);
                    return pow(-1.0, i + j + 2) * GetDeterminant(minor, m) / determinant;
                }
            ));
        }
    }
    int i = 0;
    int j = 0;
    for (auto& future : futures)
    {
        inverse_matrix[i][j] = future.get();
        if (i == n - 1) {
            i = 0;
            j++;
        }
        else
            i++;
    }
    return inverse_matrix;
}


Matrix GetInverseMatrixSingleMode(Matrix matrix, int n, double determinant)
{
    LOG_DURATION("Func time: ");
    Matrix inverse_matrix;
    InitMatrix(inverse_matrix, n, false);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            int m = n - 1;
            Matrix minor = GetMatrixMinor(matrix, n, m, i, j);
            inverse_matrix[i][j] = pow(-1.0, i + j + 2) * GetDeterminant(minor, m) / determinant;
        }
    }
    return inverse_matrix;
}

double GetDeterminantParallelMode(Matrix matrix, int n)
{
    LOG_DURATION("Func time: ");
    if (n < 1) {
        cout << "Wrong matrix size!!!" << endl;
        return 0;
    }
    else if (n == 1)
        return matrix[0][0];
    else if (n == 2)
        return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
    else {
        vector<future<double>> futures;
        futures.clear();

        double determinant = 0;
        int degree = 1;

        for (int i = 0; i < n; i++) {
            int m = n - 1;
            futures.push_back(async([&matrix, n, i, m, degree]
                {
                    Matrix minor = GetMatrixMinor(matrix, n, m, 0, i);
                    return degree * matrix[0][i] * GetDeterminant(minor, m);
                }
            ));
            degree = -degree;
        }

        for (auto& future : futures)
            determinant += future.get();

        return determinant;
    }
}

double GetDeterminantSingleMode(Matrix matrix, int n) {
    LOG_DURATION("Func time: ");
    return GetDeterminant(matrix, n);
}

void UserMode() {
    Matrix matrix, inverse_matrix;
    int parrallel_mode_flag, n;
    double determinant;

    cout << "Go parallel mode? (0 - single mode): ";
    cin >> parrallel_mode_flag;
    cout << "Imput matrix size: ";
    cin >> n;

    InitMatrix(matrix, n);
    cout << matrix;

    if (parrallel_mode_flag)
    {
        LOG_DURATION("Func time: ");
        determinant = GetDeterminantParallelMode(matrix, n);
    }
    else
    {
        LOG_DURATION("Func time: ");
        determinant = GetDeterminantSingleMode(matrix, n);
    }
    cout << "Determinant = " << determinant << endl;

    if (determinant) {
        LOG_DURATION("Func time: ");
        if (parrallel_mode_flag)
            inverse_matrix = GetInverseMatrixParallelMode(matrix, n, determinant);
        else
            inverse_matrix = GetInverseMatrixSingleMode(matrix, n, determinant);
    }
    else
        cout << "Because matrix determinant = 0,\nthen the matrix is degenerate and has no inverse!!!" << endl;
    cout << TransponateMatrix(inverse_matrix, n);
}

void TestMode() {
    size_t start_val, end_val;
    cout << "Input start matrix size: ";
    cin >> start_val;
    cout << "Input end matrix size: ";
    cin >> end_val;

    for (size_t i = start_val; i <= end_val; i++) {
        Matrix matrix, inverse_matrix;
        int n = i;
        int parrallel_mode_flag;
        double determinant;

        cout << "Imput matrix size: " << n << endl;
        InitMatrix(matrix, n);
        cout << matrix;
        // single thread mode
        cout << "Single mode" << endl;
        determinant = GetDeterminantSingleMode(matrix, n);
        cout << "Determinant = " << determinant << endl;
        if (determinant)
            inverse_matrix = GetInverseMatrixSingleMode(matrix, n, determinant);
        else
            cout << "Because matrix determinant = 0,\nthen the matrix is degenerate and has no inverse!!!" << endl;
        cout << TransponateMatrix(inverse_matrix, n);
        Sleep(10 * 1000); // после нагрева ноутбука или результаты исказятся
        // parallel mode
        cout << "Parallel mode" << endl;
        determinant = GetDeterminantParallelMode(matrix, n);
        cout << "Determinant = " << determinant << endl;

        if (determinant)
            inverse_matrix = GetInverseMatrixParallelMode(matrix, n, determinant);
        else
            cout << "Because matrix determinant = 0,\nthen the matrix is degenerate and has no inverse!!!" << endl;
        cout << TransponateMatrix(inverse_matrix, n);
        Sleep(10 * 1000); // после нагрева ноутбука или результаты исказятся
    }
}


void main()
{
    bool user_mode_flag;
    cout << "Go test mode? (0 - user mode): ";
    cin >> user_mode_flag;
   
    if (user_mode_flag)
        TestMode();
    else
        UserMode();
}